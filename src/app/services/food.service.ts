import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor(private http: HttpClient) { }

  getFoods() {
    return this.http.get(`${environment.apiUrl}/food`);
  }

  createFood(food: any) { 
    return this.http.post(`${environment.apiUrl}/food`, food);
  }

  search(termino: string) {
    return this.http.get(`${environment.apiUrl}/search/${termino}`);
  }

  deleteFood(idFood: string) {
    return this.http.delete(`${environment.apiUrl}/food/${idFood}`);
  }

  updateFood(idFood: string, food: any) {
    return this.http.put(`${environment.apiUrl}/food/${idFood}`, food);
  }
}
