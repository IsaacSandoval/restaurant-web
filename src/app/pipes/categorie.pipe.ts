import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'categorie'
})
export class CategoriePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    switch (value) {
      case 1:
        return 'Carnes';
        break;
      case 2:
        return 'Ensaladas';
        break;
      case 3:
        return 'Postres';
        break;
      case 4:
        return 'Bebidas';
        break;
      case 5:
        return 'Combos';
        break;
      default:
        return 'Sin categoria';
        break;
    }
  }

}
