import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { MatCardModule } from '@angular/material/card';
import { FoodInfoComponent } from './modals/food-info/food-info.component';
import { MatDialogModule } from '@angular/material/dialog';
import { RegisterFoodComponent } from './modals/register-food/register-food.component';
import { MatSelectModule } from '@angular/material/select';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoriePipe } from '../pipes/categorie.pipe';
import { DeleteFoodComponent } from './modals/delete-food/delete-food.component';
import {MatListModule} from '@angular/material/list';

const pagesRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  }
];

@NgModule({
  declarations: [
    HomeComponent,
    PagesComponent,
    FoodInfoComponent,
    RegisterFoodComponent,
    CategoriePipe,
    DeleteFoodComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    HttpClientModule,
    FormsModule,
    MatListModule,
    ReactiveFormsModule,
    RouterModule.forRoot(pagesRoutes)
  ],
  exports: [
    PagesComponent,
    HomeComponent
  ],
  entryComponents: [
    FoodInfoComponent,
    RegisterFoodComponent,
    DeleteFoodComponent
  ]
})
export class PagesModule { }
