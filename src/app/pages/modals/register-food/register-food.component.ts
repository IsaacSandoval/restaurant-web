import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FoodService } from 'src/app/services/food.service';
import Swal from 'sweetalert2'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-register-food',
  templateUrl: './register-food.component.html',
  styleUrls: ['./register-food.component.scss']
})
export class RegisterFoodComponent implements OnInit {

  foodForm: FormGroup;
  constructor(private foodService: FoodService,
              private dialogRef: MatDialogRef<RegisterFoodComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.foodForm = new FormGroup({
      name: new FormControl('', Validators.required),
      price: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      categorie: new FormControl('', Validators.required),
      img: new FormControl('')
    });


    if (this.data != null) {
      this.setForm();
    }
  }

  setForm() {
    this.foodForm.patchValue({
      name: this.data.food.name,
      price: this.data.food.price,
      description: this.data.food.description,
      categorie: this.data.food.categorie,
      img: this.data.food.img
    });
  }


  createFood() {
    this.foodForm.value.categorie = parseInt(this.foodForm.value.categorie, 10);
    this.foodService.createFood(this.foodForm.value).subscribe(resp => {
      Swal.fire({
        icon: 'success',
        title: 'Hecho',
        text: 'La comida ha sido creada correctamente',
      });

      this.dialogRef.close(true);
    });
  }

  updateFood() {
    this.foodForm.value.categorie = parseInt(this.foodForm.value.categorie, 10);
    this.foodService.updateFood(this.data.food._id, this.foodForm.value).subscribe(resp => {
      Swal.fire({
        icon: 'success',
        title: 'Hecho',
        text: 'La comida ha sido actualizada correctamente',
      });

      this.dialogRef.close(true);
    });
  }

  closeDialog() {
    this.dialogRef.close(false);
  }
}
