import { Component, OnInit } from '@angular/core';
import { FoodService } from 'src/app/services/food.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { RegisterFoodComponent } from '../register-food/register-food.component';

@Component({
  selector: 'app-delete-food',
  templateUrl: './delete-food.component.html',
  styleUrls: ['./delete-food.component.scss']
})
export class DeleteFoodComponent implements OnInit {
  foods: any;
  constructor(private foodService: FoodService,
              private dialogRef: MatDialogRef<DeleteFoodComponent>,
              private dialog: MatDialog
              ) { }

  ngOnInit() {
   this.getFoods();
  }

  getFoods() {
    this.foodService.getFoods().subscribe(resp => {
      this.foods = resp;
    });
  }

  deleteFood(idFood: string) {
    this.foodService.deleteFood(idFood).subscribe(resp => {
      this.getFoods();
      this.dialogRef.close(true);
    });
  }

  editFood(food: any) {
    const dialog = this.dialog.open(RegisterFoodComponent, {
      width: '800px',
      data: {
        food
      }
    });

    dialog.afterClosed().subscribe(resp => {
      if (resp === true) {
        this.dialogRef.close(true);
      }
    });
  }

}
