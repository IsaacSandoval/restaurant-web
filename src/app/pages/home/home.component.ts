import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FoodInfoComponent } from '../modals/food-info/food-info.component';
import { FoodService } from 'src/app/services/food.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnChanges {

  foods: any = [];

  foodWithFilters = [];
  @Input() public food: any;
  @Input() public newFood: boolean;
  constructor(private dialog: MatDialog, private foodService: FoodService) { }

  ngOnInit() {
    this.foodWithFilters = [];
    this.getFoods();
  }

  ngOnChanges() {
    this.foods = this.food;
    if (this.newFood === true) {
      this.getFoods();
    }
  }

  applyFilters(categorie: number) {
    this.foodWithFilters = [];
    this.foods.forEach(food => {
      if (food.categorie === categorie) {
        this.foodWithFilters.push(food);
      }
    });
  }

  async getFoods() {
    await this.foodService.getFoods().subscribe(resp => {
      this.foods = resp;
      console.log(this.foods);
    });
  }

  openFoodInfoDialog(food: any) {
    this.dialog.open(FoodInfoComponent, {
      width: '800px',
      height: '600px',
      data: {
        food
      },
      panelClass: 'modal'
    }
    );
  }

}
