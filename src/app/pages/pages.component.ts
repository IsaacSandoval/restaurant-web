import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RegisterFoodComponent } from './modals/register-food/register-food.component';
import { FoodService } from '../services/food.service';
import { DeleteFoodComponent } from './modals/delete-food/delete-food.component';
import { HomeComponent } from './home/home.component';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  foods: any = [];
  newFoodStatus: boolean = false;
  termino: string = '';
  @ViewChild(HomeComponent, null) home: HomeComponent;

  constructor(private dialog: MatDialog, private foodService: FoodService) { }

  ngOnInit() {
  }

  busqueda(termino) {
    this.termino = termino;
    if (termino === '') {
      this.newFoodStatus = true;
    }
    this.foodService.search(termino).subscribe(resp => {
      this.foods = resp;
      this.newFoodStatus = false;
    });
  }

  openRegisterDialog() {
    this.newFoodStatus = true;
    const dialog = this.dialog.open(RegisterFoodComponent, {
      width: '800px',
    });

    dialog.afterClosed().subscribe(resp => {
      if (resp === true) {
        this.home.getFoods();
      }
    });
  }

  openDeleteDialog() {
    this.newFoodStatus = true;
    const dialog = this.dialog.open(DeleteFoodComponent, {
      width: '800px',
    });

    dialog.afterClosed().subscribe(resp => {
      if (resp === true) {
        this.home.getFoods();
      }
    });
  }
}
